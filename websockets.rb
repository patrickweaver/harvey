get "/lock/ws" do
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env, nil, {ping: 15})

    ws.on :open do |event|
      p [:open, ws.object_id]
      settings.clients[:lock] << ws
    end
    
    ws.on :message do |event|
      p [:message, ws.object_id, event.data]
      msg = JSON.parse(event.data)
      if msg["type"] == "lock_request"
        logger.info "Got lock request on row #{msg['id']} by #{@user.username}"
        person = Person.find(msg["id"].to_i)
        if person.lock_ends_at.nil? || person.lock_ends_at - DateTime.now < 0 || person.locked_by.nil?
          person.locked_by = nil
          person.lock_ends_at = nil
          person.save
          person = Person.find(msg["id"].to_i)
        end
        if person.locked_by.nil?
          ws.send({type: "lock_request", status: 1}.to_json)
          # Allocates a 10 minute (600 second) lock
          person.lock_ends_at = DateTime.now + Rational(600, 60*60*24)
          person.locked_by = @user.id
          person.save
          person = Person.find(msg["id"].to_i)
          logger.info "Set lock_ends_at to #{DateTime.now + Rational(600, 60*60*24)} for #{@user}"
          Thread.new do
            logger.info "Lock time remaining: #{(person.lock_ends_at - DateTime.now)*24*60*60 > 0}; Lock claimed? #{!person.locked_by.nil?}"
            while !person.locked_by.nil? && (person.lock_ends_at - DateTime.now)*24*60*60 > 0
              logger.info "#{(person.lock_ends_at - DateTime.now)*24*60*60} second remainig" if ((person.lock_ends_at - DateTime.now)*24*60*60).to_i % 10 == 0
              if (DateTime.now - person.lock_ends_at)*24*60*60 < 60
                ws.send({type: "lock_timeout", remaining: (person.lock_ends_at - DateTime.now).to_f*24*60*60}.to_json)
                sleep 60
              end
            end
            logger.info "NILLIFYING"
            person.locked_by = nil
            person.lock_ends_at = nil
            person.save
            person = Person.find(msg["id"].to_i)
          end
          # time = DateTime.parse(person.lock_ends_at.to_s)
          # logger.info [time, time.class].inspect
          ws.send({type: "lock_timeout", remaining: (person.lock_ends_at - DateTime.now).to_f}.to_json)
        else
          ws.send({type: "lock_request", status: 0}.to_json)
        end
        
      elsif msg["type"] == "lock_extension"
        person = Person.find(msg["id"].to_i)
        if person.locked_by == @user.id
          person.lock_ends_at = person.lock_ends_at.plus_with_duration 120
          person.save
          person = Person.find(msg["id"].to_i)
          # time = DateTime.parse(person.lock_ends_at.to_s)
          ws.send({type: "lock_timeout", remaining: (person.lock_ends_at - DateTime.now).to_f}.to_json)
        elsif
          ws.send("You do not have a lock!")
        end

      elsif msg["type"] == "lock_check"
        person = Person.find(msg["id"].to_i)
        if person.lock_ends_at.nil?
          ws.send("Nobody has a lock!")
        else
          ws.send({type: "lock_check", remaining: (person.lock_ends_at - DateTime.now).to_f}.to_json)
        end

      elsif msg["type"] == "row_update"
        person = Person.find(msg["id"].to_i)
        if person.locked_by == @user.id
          person.update_attributes Hash[Hash(msg["row"]).map { |k, v| [k.to_sym, v] }]
          person.locked_by = nil
          person.lock_ends_at = nil
          person.save
        end
      end
    end

    ws.on :close do |event|
      p [:close, ws.object_id, event.code, event.reason]
      settings.clients[:lock].delete(ws)
      ws = nil
    end

    ws.rack_response
  else
    logger.info "\n\n\n\n\n\n\nn\n\nNOT OPENING WS \n\n\n\n\n\nn\nn"
  end
end