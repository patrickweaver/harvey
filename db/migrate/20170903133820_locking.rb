class Locking < ActiveRecord::Migration[5.1]
  def change
  	add_column :people, :locked_by, :integer
  	add_column :people, :lock_ends_at, :datetime
  end
end
