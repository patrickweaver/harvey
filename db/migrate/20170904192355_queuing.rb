class Queuing < ActiveRecord::Migration[5.1]
  def change
    create_table :review_tasks do |i|
      i.datetime :created_at
      i.integer :queue_id
      i.integer :user_id
      i.integer :outcome
      i.text :person_ids
    end

    # # Will not be nill if review is complete
    # create_table :person_review_tasks do |i|
    #   i.integer :person_id
    #   i.integer :review_task_id
    # end

    create_table :review_queues do |i|
      i.datetime :created_at
      i.datetime :updated_at
      # Conditions are of the format `rowname:rexexp\n`
      # Regexp will be thrown into a ruby regexp literal
      i.text :entry_condition
      i.text :split_condition # All the record where the regexp matches downcased are equal
      i.text :outcomes
      i.text :name
    end
  end
end
