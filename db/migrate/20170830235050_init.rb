class Init < ActiveRecord::Migration[5.1]
  def change
  	create_table :people do |i|
  	  i.decimal :lat
  	  i.decimal :lng
  	  i.integer :incident_number
  	  i.text :name
  	  i.text :street_address
  	  i.text :apt
  	  i.text :city
  	  i.text :zip_code
  	  i.text :twitter_handle
  	  i.text :priority
  	  i.text :status
  	  i.text :number_of_people
  	end

  	create_table :comments do |i|
  	  i.datetime :created_at
  	  i.integer :person_id
  	  i.text :body
  	end
  end
end
