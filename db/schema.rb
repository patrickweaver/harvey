# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170904192355) do

  create_table "channels", force: :cascade do |t|
    t.text "name"
    t.text "discription"
    t.boolean "public"
    t.datetime "created_at"
  end

  create_table "comments", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "person_id"
    t.text "body"
    t.integer "user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "user_id"
    t.integer "channel_id"
    t.datetime "created_at"
    t.text "body"
  end

  create_table "people", force: :cascade do |t|
    t.decimal "lat"
    t.decimal "lng"
    t.integer "incident_number"
    t.text "name"
    t.text "street_address"
    t.text "apt"
    t.text "city"
    t.text "zip_code"
    t.text "twitter_handle"
    t.text "priority"
    t.text "status"
    t.text "number_of_people"
    t.integer "locked_by"
    t.datetime "lock_ends_at"
  end

  create_table "review_queues", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "entry_condition"
    t.text "split_condition"
    t.text "outcomes"
    t.text "name"
  end

  create_table "review_tasks", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "queue_id"
    t.integer "user_id"
    t.integer "outcome"
    t.text "person_ids"
  end

  create_table "sessions", force: :cascade do |t|
    t.text "cookie"
    t.integer "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_channels", force: :cascade do |t|
    t.integer "user_id"
    t.integer "channel_id"
    t.datetime "last_read"
  end

  create_table "users", force: :cascade do |t|
    t.text "name"
    t.text "username"
    t.text "password_hash"
    t.text "password_salt"
    t.text "oauth_token"
    t.text "oauth_provider"
  end

end
