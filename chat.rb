get "/chat/ws" do
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env, nil, {ping: 15})

    ws.on :open do |event|
      p [:open, ws.object_id]
      @thr = []
      settings.clients[:chat] << ws
    end

    ws.on :close do |event|
      p [:close, ws.object_id, event.code, event.reason]
      settings.clients[:chat].delete(ws)
      ws = nil
    end

    # Return async Rack response
    ws.rack_response
  else
    redirect back
  end
end

post "/message/create" do
  logger.info "\n\n\n#{params}\n\n"
  redirect back unless channel = @user.channels.find(params[:channel])
  channel.messages.create(user_id: @user.id, body: params[:body])
end

get "/channel/:channel_id/json" do
  chan = @user.channels.find(params[:channel_id])
  body({name: chan.name, disc: chan.discription, messages: chan.messages.last(100).map {|msg| {username: msg.user.username, body: msg.body, created_at: msg.created_at} } || []}.to_json)
end

get "/chat" do
  @channel = @user.channels.first
  erb :chat
end

post "/channel/create" do
  @user.channels.create(name: params[:name], discription: params[:disc], public: true)
  redirect "/chat"
end