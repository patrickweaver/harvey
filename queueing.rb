def gen_hash(string)
  string.split("\n").map do |row|
    col_name, regex = row.scan(%r{([^:]*):(.*)}).flatten
    [col_name.to_sym, %r{#{regex}}]
  end.to_h
end

post "/review/new" do
  a = ReviewQueue.create(entry_condition: params[:entry_condition],
                  split_condition: params[:split_condition],
                  outcomes: params[:outcomes],
                  name: params[:name])
  body a.id
end

get "/review/:id" do
  queue = ReviewQueue.find(params[:id].to_i)
  @task = queue.review_tasks.where.not(outcome: nil).first
  erb :review_tasks
end

get "/review/:id/create" do
  queue = ReviewQueue.find(params[:id].to_i)
  entry_criteria = gen_hash(queue.entry_condition)
  people = Person.all.select do |person|
    person.attributes.all? { |k,v| entry_criteria[k].nil? || v =~ entry_criteria[k] } #&& !ReviewTask.exists?(person_id: person.id, queue_id: queue.id)
  end
  tasks = {} #{{colname => matches} => [people]}
  collection_regex = gen_hash(queue.split_condition)
  people.each do |person|
    keys = person.attributes.map do |k,v|
      [k, v.to_s.scan(collection_regex[k.to_sym])] unless collection_regex[k.to_sym].nil?
    end
    keys = Hash[*keys.reject(&:nil?).each_slice(2).to_a]
    tasks[keys] ||= []
    tasks[keys].push(person)
  end
  tasks.values.each do |people_array|
    queue.review_tasks.create(person_ids: people_array.map { |i| i.id }) if people_array.length > 1
  end
end