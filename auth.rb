require "securerandom"
require "bcrypt"

set :whitelist, [%r{^[/]{0,1}$},
                 %r{^/sheet[/]{0,1}$},
                 %r{^/sheet/ws[/]{0,1}$},
                 %r{^/people[/]{0,1}\w*$},
                 %r{^/person/json/\d*$},
                 %r{^/login[/]{0,1}$}
               ]

def logged_in?
  return false unless sess = Session.find_by(cookie: session[:cookie])
  @user = sess.user
end

def new_user(username, pass)
  salt = BCrypt::Engine.generate_salt
  User.create(username:username, password_hash: BCrypt::Engine.hash_secret(pass, salt), password_salt: salt)
end

before do
  logged_in?
  # unless logged_in? || settings.whitelist.any? { |url| url =~ request.path_info }
  #   redirect "/login"
  # end
end

get "/login" do
  redirect (back.to_s.end_with?("/login") ? request.base_url : back) if logged_in?
  erb :login
end

get "/logout" do
  if logged_in?
    Session.find_by(cookie: session[:cookie]).destroy
    session.clear
  end
  redirect back
end

post "/login" do
  if !logged_in? && (usr = User.find_by(username: params[:username])) && usr.password_hash == BCrypt::Engine.hash_secret(params[:password], usr.password_salt)
    session.clear
    session[:cookie] = SecureRandom.base64
    usr.sessions.create(cookie:session[:cookie])
  end
  redirect (back.to_s.end_with?("/login") ? request.base_url : back)
end