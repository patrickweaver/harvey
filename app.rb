require "sinatra"
require "faye/websocket"
require "thin"
require "geocoder"
require_relative "database"
require_relative "db_ws_listeners"
require_relative "auth"
require_relative "chat"
require_relative "websockets"
require_relative "queueing"

enable :sessions

use Rack::Logger

Faye::WebSocket.load_adapter('thin')

set :clients, {sheet: [], chat: [], lock: []}

get "/" do
  erb :home
end

get "/sheet" do
  erb :sheet
end

get "/sheet/ws" do
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env, nil, {ping: 15})

    ws.on :open do |event|
      p [:open, ws.object_id]
      settings.clients[:sheet] << ws
    end

    ws.on :close do |event|
      p [:close, ws.object_id, event.code, event.reason]
      settings.clients[:sheet].delete(ws)
      ws = nil
    end

    # Return async Rack response
    ws.rack_response
  else
    redirect back
  end
end

get "/people" do
  people = []
  Person.all.each { |person| people.push(person.attributes) }
  people.to_json
end

get "/people/nonrescued" do
  people = []
  Person.where.not(status: "Rescued").each { |person| people.push(person.attributes) }
  people.to_json
end

get "/people/rescued" do
  people = []
  Person.where(status: "Rescued").each { |person| people.push(person.attributes) }
  people.to_json
end

get "/person/json/:id" do
  Person.find(params[:id]).to_json
end

get "/person/:id" do
  @id = params[:id]
  puts "ID=#{@id}"
  @person = Person.find(params[:id])
  erb :person
end

get "/person/create" do
  @location = request.location
  erb :form
end

post "/person/create" do
  if params[:id].nil?
    p = Person.create(lat: params[:lat], lng: params[:lng])
  else
    p = Person.find(params[:id].to_i)
    Person.update(params[:id].to_i, params.select { |k,v| %w[lat lng name incident_number street_address apt city zip_code twitter_handle priority status number_of_people].include? k})
  end
  status 200
  body [p.id.to_s]
end

get "/person/delete/:id" do
  Person.find(params[:id]).destroy
  redirect "/"
end

post "/person/:id" do
  Person.update(params[:id], params.select { |k,v| %w[lat lng name incident_number street_address apt city zip_code twitter_handle priority status number_of_people].include? k})
  redirect "/"
end

post "/comment/create" do
  Comment.create(person_id: params[:person_id], body: params[:body])
  redirect back
end

post "/search" do
  body = JSON.parse(request.body.read)
  attribute_queries = Person.column_names.map { |a| "#{a} LIKE '%#{body[a.to_s]}%'" unless body[a.to_s].nil? || body[a.to_s].empty? }.reject(&:nil?).reject(&:empty?)
  Person.where(attribute_queries.join(' AND ')).to_json
end

get "/search" do
  erb :search
end
