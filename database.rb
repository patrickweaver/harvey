require "sinatra/activerecord"

ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :database  => "foo.sqlite3"
)

class Person < ActiveRecord::Base
  has_many :comments
end

class Comment < ActiveRecord::Base
  belongs_to :person
end

class Session < ActiveRecord::Base
  belongs_to :user
end

class UserChannel < ActiveRecord::Base
  belongs_to :channel
  belongs_to :user
end

class User < ActiveRecord::Base
  has_many :sessions
  has_many :comments
  has_many :messages
  has_many :user_channels
  has_many :channels, through: :user_channels
  has_many :review_tasks
end

class Channel < ActiveRecord::Base
  has_many :user_channels
  has_many :users, through: :user_channels
  has_many :messages
end

class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :channel
end

class ReviewTask < ActiveRecord::Base
  serialize :person_ids, Array
  belongs_to :user
  belongs_to :review_queue
end

class ReviewQueue < ActiveRecord::Base
  has_many :review_tasks, foreign_key: :queue_id
  serialize :outcomes, Array
end
